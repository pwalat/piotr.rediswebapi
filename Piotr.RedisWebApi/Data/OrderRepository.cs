﻿using System;
using System.Collections.Generic;
using System.Linq;
using Piotr.RedisWebApi.Models;
using ServiceStack.Common.Extensions;
using ServiceStack.Redis;

namespace Piotr.RedisWebApi.Data
{
    public class OrderRepository : IOrderRepository
    {
        private readonly IRedisClient _redisClient;

        public OrderRepository(IRedisClient redisClient)
        {
            _redisClient = redisClient;
        }

        public IList<Order> GetCustomerOrders(Guid customerId)
        {
            using (var orderClient = _redisClient.GetTypedClient<Order>())
            {
                var orderIds = _redisClient.GetAllItemsFromSet(RedisKeys
                            .GetCustomerOrdersReferenceKey(customerId));
                IList<Order> orders = orderClient.GetByIds(orderIds);
                return orders;
            }
        }

        public IList<Order> StoreAll(Customer customer, IList<Order> orders)
        {
            foreach (var order in orders)
            {
                if (order.Id == default(Guid))
                {
                    order.Id = Guid.NewGuid();
                }
                order.CustomerId = customer.Id;
                if (!customer.Orders.Contains(order.Id))
                {
                    customer.Orders.Add(order.Id);
                }

                order.Lines.ForEach(l=>_redisClient
                    .IncrementItemInSortedSet(RedisKeys.BestSellingItems,
                                                                     (string) l.Item, (long) l.Quantity));
            }
            var orderIds = orders.Select(o => o.Id.ToString()).ToList();
            using (var transaction = _redisClient.CreateTransaction())
            {
                transaction.QueueCommand(c => c.Store(customer));
                transaction.QueueCommand(c => c.StoreAll(orders));
                transaction.QueueCommand(c => c.AddRangeToSet(RedisKeys
                    .GetCustomerOrdersReferenceKey(customer.Id),
                    orderIds));
                transaction.Commit();
            }

            return orders;
        }

        public Order Store(Customer customer, Order order)
        {
            IList<Order> result = StoreAll(customer, new List<Order>() { order });
            return result.FirstOrDefault();
        }

        public IDictionary<string, double> GetBestSellingItems(int count)
        {
            return _redisClient
                .GetRangeWithScoresFromSortedSetDesc(RedisKeys.BestSellingItems, 
                0, count - 1);
        }
    }
}