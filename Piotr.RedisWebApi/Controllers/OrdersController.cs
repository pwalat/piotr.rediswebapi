﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using Piotr.RedisWebApi.Data;
using Piotr.RedisWebApi.Models;

namespace Piotr.RedisWebApi.Controllers
{
    public class OrdersController : ApiController
    {
        public IOrderRepository OrderRepository { get; set; }
        public ICustomerRepository CustomerRepository { get; set; }

        public HttpResponseMessage Post([FromBody] Order order)
        {
            var customer = CustomerRepository.Get(order.CustomerId);
            var result = OrderRepository.Store(customer, order);
            return Request.CreateResponse(HttpStatusCode.Created, result);
        }

        [ActionName("top")]
        [HttpGet]
        public IDictionary<string, double> GetBestSellingItems(int count)
        {
            return OrderRepository.GetBestSellingItems(count);
        }


        [ActionName("customer")]
        [HttpGet]
        public IList<Order> GetCustomerOrders(Guid id)
        {
            return OrderRepository.GetCustomerOrders(id);
        }
    }
}